RailsAdmin.config do |config|

  ### Popular gems integration
  #config.authorize_with do
  #  redirect_to main_app.root_path unless warden.user.admin == true
  #end
  
  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar true
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new do
      except ["General", "Option", "Page"]
    end
    export
    bulk_delete do
      except ["General", "Option", "Page"]
    end
    show
    edit
    delete do
      except ["General", "Option", "Page"]
    end
    show_in_app
    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.model Page do
    edit do
      field :value, :wysihtml5 do
        config_options toolbar: { fa: true }, # use font-awesome instead of glyphicon
                       html: true, # enables html editor
                       parserRules: { tags: { p:1 } }
      end

    end
  end
end