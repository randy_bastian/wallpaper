class CreateKeywords < ActiveRecord::Migration[5.0]
  def up
    create_table :keywords do |t|
    	t.text 		"title"
    	t.timestamps
    end
  end

  def down
  	drop_table :keywords
  end
end
