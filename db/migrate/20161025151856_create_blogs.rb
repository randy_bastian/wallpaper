class CreateBlogs < ActiveRecord::Migration[5.0]
  def up
    create_table :blogs do |t|
    	t.string	"title"
    	t.text		"description"
    	t.text		"keyword"
    	t.string	"featured_image"
    	t.timestamps
    end
  end

  def down
  	drop_table :blogs
  end
end
