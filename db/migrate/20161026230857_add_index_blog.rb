class AddIndexBlog < ActiveRecord::Migration[5.0]
  def change
  	add_index :blogs, :title,                unique: true
  end
end
