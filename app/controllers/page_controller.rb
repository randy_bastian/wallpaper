class PageController < ApplicationController
	layout "home"
  include ViewHelper
  def index
    global()
  	title 		= params[:title]
  	allpage		= Page.all
  	allpage.each do |mypage|
  		if mypage.title.parameterize == title
  			@thispage = mypage
  		end
  	end
    @web_title = @thispage.title
    @general_description = @web_title + " - " + @general_description
  end
end
