class BlogController < ApplicationController
	layout "home"
	include ViewHelper
 	def index
 		global()
 		if params[:page].nil?
 			@this_page = 1
 		else
 			@this_page = params[:page].to_i
 		end
 		offset_query 	= (@this_page - 1) * 6
 		@blog 			= Blog.limit(6).offset(offset_query).sort_by {|h| -h[:id]}
 		@pagination		= Blog.count / 6 + 1
 		@web_title 		= "Blog Page #{@this_page}"
 		@general_description = @web_title + " | " + @general_description
 		@general_keyword = @web_title + "," + @general_keyword
	end

	def read
		global()
		@id_artikel		= params[:artikel].to_i
		@blog_choose 	= Blog.find(@id_artikel)
		@imageArray		= @blog_choose.isis
		@recent_post 	= Blog.last(5).reverse
		## SEO
		@web_title 		= @blog_choose.title
		@general_keyword = @blog_choose.keyword
		@general_description = @blog_choose.description
		@general_meta_image = @blog_choose.featured_image
	end
end
