// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery.min
//= require bootstrap.min
//= require owl.carousel.min
//= require bg-moving
//= require jquery.mCustomScrollbar.concat.min
//= require custom
//= require lazyload
//= require social-share-button

$(function() {
    $("img.lazy").lazyload();
});

$(".form_submit").click(function(){
  var button     = $(this);
  var form       = button.closest("form");
  var action     = form.attr("action");
  var text_field = form.find(".key");
  var value      = text_field.val();

  window.location = action + "/" + value;
  return false;
});