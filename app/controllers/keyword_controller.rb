class KeywordController < ApplicationController
	before_action :authenticate_user!
	layout "home"
  	def index

  	end

  	def create
  		string_array = params[:title].split("\r\n")
  		string_array.each do |arr|
  			Keyword.create(:title => arr)
  		end
  		redirect_to :action => "index"
 	end
end
