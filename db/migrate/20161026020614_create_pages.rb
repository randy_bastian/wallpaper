class CreatePages < ActiveRecord::Migration[5.0]
  def up
    create_table :pages do |t|
    	t.string "title"
    	t.text "value"
      t.timestamps
    end
    Page.create :title => "Contact Us"	, :value => ""
    Page.create :title => "DMCA"		, :value => ""
    Page.create :title => "Privacy Policy", :value => ""
  end

  def down
  	drop_table :pages
  end
end
