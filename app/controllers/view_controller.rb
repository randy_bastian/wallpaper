class ViewController < ApplicationController
	#before_action :authenticate_user!
	layout "home"
	include ViewHelper
	## Fresh Wallpaper
	def index
		global()
		## Web title
		page = params[:page]
		@web_title = "Fresh Wallpaper"
		@pagi_link = "fresh"
		@general_keyword = @web_title + "," + @general_keyword
		## Get HTML
		if page.nil?
			url = "https://wallpaperscraft.com/all"
			page = 1
		else
			url = "https://wallpaperscraft.com/all/page#{page}"
			url = url_escape(url)
			@web_title = "Fresh Wallpaper" + " | Page #{page}"
		end
		scrape(url)
		@general_description = @web_title + " Page: #{page} - " + @general_description
	end
	## Popular Wallpaper
	def popular
		global()
		@web_title = "Popular Wallpaper"
		@pagi_link = "popular"
		page = params[:page]
		@general_keyword = @web_title + "," + @general_keyword
		if page.nil?
			url = "https://wallpaperscraft.com/all/downloads"
			page = 1
		else
			url = "https://wallpaperscraft.com/all/downloads/page#{page}"
			url = url_escape(url)
			@web_title = "Popular Wallpaper" + " | Page #{page}"
		end
		scrape(url)
		@general_description = @web_title + " Page: #{page} - " + @general_description
		render("index")
	end

	def category
		global()
		@web_title = "Category: " + params[:name]
		@pagi_link = "category/#{params[:name]}"
		name_category 	= params[:name]
		page 			= params[:page]

		if page.nil?
			url = "https://wallpaperscraft.com/catalog/#{name_category}/date"
			url = url_escape(url)
			page = 1
		else
			url = "https://wallpaperscraft.com/catalog/#{name_category}/date/page#{page}"
			url = url_escape(url)
		end
		scrape(url)
		@general_keyword = @web_title + "," + @general_keyword
		@web_title = @web_title + " | Page: " + page.to_s
		@general_description = @web_title + " - " + @general_description
		render("index")
	end

	def tag
		global()
		@web_title = "Tag: " + params[:name]
		@pagi_link = "tag/#{params[:name]}"
		name_category 	= params[:name]
		page 			= params[:page]

		if page.nil?
			url = "https://wallpaperscraft.com/tag/#{name_category}/date"
			url = url_escape(url)
			page = 1
		else
			url = "https://wallpaperscraft.com/tag/#{name_category}/date/page#{page}"
			url = url_escape(url)
		end
		scrape(url)
		@web_title = @web_title + " | Page:" + page.to_s
		@general_description = @web_title + " - " + @general_description
		@general_keyword = @web_title + "," + @general_keyword
		render("index")
	end

	def search
		global()
		@web_title 	= params[:keyword]
		@pagi_link 	= "pics/#{params[:keyword]}"
		keyword 	= params[:keyword]
		page 		= params[:page]

		if page.nil?
			url = "https://wallpaperscraft.com/search/keywords?q=#{keyword}"
			url = url_escape(url)
			page = 1
		else
			url = "https://wallpaperscraft.com/search/keywords?q=#{keyword}/page#{page}"
			url = url_escape(url)
		end
		scrape(url)
		@web_title = @web_title + " | Page: " + page.to_s
		@general_description = @web_title + " - " + @general_description
		@general_keyword = @web_title + "," + @general_keyword
		render("index")
	end

	def picture
		global()
		keyword 	= params[:keyword]
		@link_down 	= keyword
		url 		= "https://wallpaperscraft.com/wallpaper/#{keyword}"
		scrape_view(url)
		@general_description = @web_title + " - " + @general_description
		@general_keyword 	= @web_title
		@general_meta_image = @photo_file
	end
end
