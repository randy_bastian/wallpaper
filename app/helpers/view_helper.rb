module ViewHelper
	## Class untuk entry data search
	class Entry
		def initialize(title, link, image, unduh)
			@title 	= title
			@link 	= link
			@image 	= image
			@unduh 	= unduh
		end
		attr_reader :title
		attr_reader :link
		attr_reader :image
		attr_reader :unduh
	end
	## Class untuk Pagination
	class Pagination
		def initialize(number, type)
			@number 	= number
			@type		= type
		end
		attr_reader :number
		attr_reader :type
	end
	## Perbaiki Link
	def url_escape(url)
		require 'uri'
		URI.encode(url)
	end
	## Class untuk keyword di halaman
	class Keyword
		def initialize(title,type)
			@title	= title
			@type 	= type
		end
		attr_reader :title
		attr_reader :type
	end
	## Action untuk scrape website
	def scrape(link)
		## Get HTML
		require 'open-uri'
		doc = $redis.get(link)
		if doc.nil?
			doc = Nokogiri::HTML(open(link))
			$redis.set(link,doc)
		else
			doc = Nokogiri::HTML.parse(doc)
		end
		#Get Halaman
		entries = doc.css(".wallpaper_pre")
		@entriesArray = []
		entries.each do |entry|
			title 	= entry.css(".pre_name > a").text
			link	= entry.css(".pre_name > a")[0]["href"]
			link	= link.sub("//wallpaperscraft.com/wallpaper/","")
			image	= entry.css("a > img")[0]["src"]
			unduh 	= entry.css(".pre_info > .pre_count").text
			@entriesArray << Entry.new(title,link,image,unduh)
		end

		## List Page Semua
		pag 			= doc.css(".page_select")
		@pagiArray 		= []
		pag.each do |pa|
			@pagiArray << Pagination.new(pa.text().to_i , "page_pasif")
		end

		## Tambahkan active page ke array page
		@pagiArray	<< Pagination.new(doc.css(".page_active").text.to_i , "active")

		## Ambil keyword di bagian bawah halaman
		keyword = doc.css(".tags_in > a")
		@keywordArray = []
		keyword.each do |key|
			title 	= key.text()
			type 	= key["class"]
			@keywordArray << Keyword.new(title,type)
		end
	end
	## Scrape View
	def scrape_view(link)
		require 'open-uri'
		doc = $redis.get(link)
		if doc.nil?
			doc = Nokogiri::HTML(open(link))
			$redis.set(link,doc)
		else
			doc = Nokogiri::HTML.parse(doc)
		end
		## Web title
		@web_title 		= doc.css(".page_name > h1").text
		## Foto Preview
		@photo_file 	= doc.css(".wb_preview > a > img")[0]["src"]
		## Tag Foto
		photo_tag		= doc.css(".wb_tags > a")
		@photo_tagArray = []
		photo_tag.each do |tag|
			title 	=	tag.text()
			type 	= 	tag["href"]
			type 	= 	type.sub("//wallpaperscraft.com/tag/","")
			@photo_tagArray << Keyword.new(title,type)
		end
		## Original Resolution
		@ori_resolusi	= doc.css(".wb_more > a")[0].text
		## Download Information
		@download_info 	= doc.css(".wb_more > a")[1].text
		### WideScreen Resolusi
		@photo_resolusiArray = []
		photo_reso 		= doc.css(".wb_res_cat_raz")[2].css("a")
		photo_reso.each do |reso|
			@photo_resolusiArray << reso.text
		end
		## SImilar Wallpaper
		similar_photo	= doc.css(".wallpaper_pre_wb")
		@similar_photoArray = []
		similar_photo.each do |simi|
			break if @similar_photoArray.length == 3
			title 	= simi.css("a")[0]["title"]
			link 	= simi.css("a")[0]["href"]
			link 	= link.sub("//wallpaperscraft.com/wallpaper/","")
			image	= simi.css("a > img")[0]["src"]
			@similar_photoArray << Entry.new(title,link,image,"")
		end

		## Ambil keyword di bagian bawah halaman
		keyword = doc.css(".tags_in > a")
		@keywordArray = []
		keyword.each do |key|
			title 	= key.text()
			type 	= key["class"]
			@keywordArray << Keyword.new(title,type)
		end
	end	

	def global
		## SEtting global Header untuk SEO
		general 			= General.first
		@general_title 		= general.title
		@general_keyword 	= general.keyword 
		@general_description = general.description
		@general_image 		= general.asset.url
		@general_meta_image = general.asset.url
		## Setting category untuk header
		@allcategory 		= Category.all.sort_by {|h| -h[:title]}
		## setting Header,Footer Ads & Script
		options 			= Option.first
		@header_script 		= options.header_script
		@header_ads 		= options.header_ads
		@footer_script 		= options.footer_script
		@footer_ads 		= options.footer_ads
	end
end
