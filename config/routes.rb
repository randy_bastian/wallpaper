Rails.application.routes.draw do
  
  root "home#index"
  get 'keyword/index'
  resources :keyword
  get 'admin/keyword/new', to: "keyword#index"

  get 'page(/:title)', to: "page#index"
  get 'blog(/:page)', to: "blog#index"
  get 'blog/read(/:artikel)', to: "blog#read"

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users
  
  get "fresh(/:page)", to: "view#index"
  get "popular(/:page)", to: "view#popular"
  get "category(/:name(/:page))", to: "view#category"
  get "tag(/:name(/:page))", to: "view#tag"
  get "pics(/:keyword(/:page))", to: "view#search"
  get "pic(/:keyword)", to: "view#picture"

  match ':controller(/:action(/:id))', :via => :get
  get '*path' => redirect('/')
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
