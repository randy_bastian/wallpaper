class CreateIsis < ActiveRecord::Migration[5.0]
  def up
    create_table :isis do |t|
      t.integer "blog_id"
    	t.string	"link"
    	t.string	"title"
    	t.string	"image_link"
      t.timestamps
    end
    add_index("isis","blog_id")
  end

  def down
    remove_index("isis","blog_id")
  	drop_table :isis
  end
end
