# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://wallpaper27.me"
SitemapGenerator::Sitemap.create_index = true

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end
  Page.find_each do |pages|
    mypath = pages.title.parameterize
    add "/page/#{mypath}", :lastmod => pages.updated_at, :priority => 0.8 , :changefreq => 'monthly'
  end
  Blog.find_each do |blogs|
    mypath = blogs.id.to_s + "-" + blogs.title.parameterize
    add "/blog/read/#{mypath}", :lastmod => blogs.updated_at, :priority => 0.9, :changefreq => 'daily'
  end
  Category.find_each do |cate|
    mypath = cate.title.parameterize
    add "/category/#{mypath}", :lastmod => cate.updated_at, :priority => 0.8, :changefreq => 'weekly'
  end
  add "/popular", :priority => 0.8, :changefreq => 'daily'
  add "/fresh", :priority => 0.9, :changefreq => 'daily'
end
